﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class GameController : MonoBehaviourPun
{
	public static GameController Instance { get; private set; }

	public ScenariosConfig scenariosConfig;
	public HeroesConfig heroesConfig;

	public static Scenarios scenario;
	public static Heroes hero;

	List<Transform> bornPlaces;

	public static GameObject player;

	[Header("Buttons Config")]
	public EventTriggerFunctions buttonAtk;
	public EventTriggerFunctions buttonMove, buttonSkill;
	public Joystick joystick { get; private set; }

	public GameObject[] hudNPC, hudPlayer;

	public Image[] playerBar;

	/*[SerializeField]
	GameObject watchScreen;*/

	int numOfPlayers = 0;
	public static int myNum;

	void Awake()
	{
		Instance = GetComponent<GameController>();

		joystick = buttonMove.GetComponent<Joystick>();

		for(int i = 0; i < playerBar.Length; i++)
		{
			playerBar[i].gameObject.SetActive(false);
		}

		string scenarioName = scenariosConfig.scenarios[1].scenarioName;
		scenario = SelectMap.FindScenario(scenarioName);
	}

	void Start()
	{

	}

	void Update()
	{

	}

	public void PreparingGame()
	{
		scenario.scenario = Instantiate(scenario.scenario, ControlScreens.instance.game.transform);
		scenario.scenario.transform.localPosition = Vector3.zero;
		scenario.scenario.transform.localEulerAngles = Vector3.zero;

		bornPlaces = scenario.GetBornPlaces();

		Transform bornPlace = bornPlaces[myNum - 1];

		player = PhotonNetwork.Instantiate(hero.hero.name, bornPlace.position, bornPlace.rotation);
		player.GetPhotonView().RPC("Initialize", RpcTarget.AllBuffered, (myNum -1));

		Vector3 pointPos = bornPlace.position;
	}

	[PunRPC]
	void RemovePlayer()
	{
		//Mark presence
		numOfPlayers--;

		if (numOfPlayers <= 1)
		{
			print("You Win");
		}
		else
		{
			print("You Loose");
		}

		Goodbye();
	}

	public void Goodbye()
	{
		WinScreen winScreen = ControlScreens.instance.winScreen.GetComponent<WinScreen>();
		winScreen.Active(player.activeInHierarchy, hero);
	}
}
