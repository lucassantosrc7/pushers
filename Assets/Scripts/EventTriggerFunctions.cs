﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class EventTriggerFunctions : EventTrigger
{
	/// <summary>
	/// https://docs.unity3d.com/es/530/ScriptReference/EventSystems.EventTrigger.html
	/// </summary>

	public enum Functions
    {
        Null, Down, Enter, Exit
    }

    [HideInInspector]
    public Functions state = Functions.Null;

    [HideInInspector]
    public PointerEventData eventData = null;

    public override void OnPointerDown(PointerEventData data)
    {
        state = Functions.Down;
    }
	public override void OnPointerEnter(PointerEventData data)
    {
        eventData = data;
        state = Functions.Enter;
    }
    public override void OnPointerExit(PointerEventData data)
    {
        eventData = null;
        state = Functions.Exit;
    }
}