﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Movement))]
[RequireComponent(typeof(DetectHit))]
[RequireComponent(typeof(Attack))]
public class Player : MonoBehaviourPun
{
	#region PlayerComponents
	[SerializeField]
	Camera playerCam;

	[HideInInspector]
	public EventTriggerFunctions buttonAtk, buttonMove, buttonSkill;

	public PlayerConfig character;

	public Movement movement { get; private set; }
	public DetectHit detectHit { get; private set; }
	public Attack attack { get; private set; }

	public Rigidbody rb { get; private set; }
	public AudioSource source { get; private set; }
	public Animator anim { get; private set; }
	#endregion

	public Image photo { get; private set; }
	public Text percent { get; private set; }

	void Awake()
	{
		#region Get components
		movement = GetComponent<Movement>();
		detectHit = GetComponent<DetectHit>();
		attack = GetComponent<Attack>();

		rb = GetComponent<Rigidbody>();
		source = GetComponent<AudioSource>();
		#endregion

		#region Set components
		buttonAtk = new EventTriggerFunctions();
		buttonMove = new EventTriggerFunctions();
		buttonSkill = new EventTriggerFunctions();

		buttonAtk.state = EventTriggerFunctions.Functions.Down;

		movement.player = this;
		detectHit.player = this;
		attack.player = this;
		#endregion
	}

	[PunRPC]
	public void Initialize(int myNum)
	{
		Vector3 rot = transform.eulerAngles;
		transform.eulerAngles = Vector3.zero;
		transform.SetParent(ControlScreens.instance.game.transform);

		character.transform.localEulerAngles = rot;
		anim = character.GetComponent<Animator>();

		photo = GameController.Instance.playerBar[myNum];
		percent = photo.GetComponentInChildren<Text>();

		photo.sprite = character.photo;
		percent.text = "0%";

		photo.gameObject.SetActive(true);

		if (photonView.IsMine)
		{
			Instantiate(playerCam, transform);
		}

		character.CheckChange(this);
	}

	void Start()
	{
		if(photo != null && photonView.IsMine)
		{
			photo.transform.localScale = Vector3.one * 1.3f;
		}
	}

	void Update()
	{
        
	}

	public static bool CheckDisable(Player player)
	{
		if(player == null || player.character == null)
		{
			return false;
		}

		return true;
	}

	public static void PlayerSound(AudioClip[] clips, AudioSource _source)
	{
		if (clips.Length > 0 && !_source.isPlaying)
		{
			_source.PlayOneShot(clips[Random.Range(0, clips.Length)]);
		}
	}
}
