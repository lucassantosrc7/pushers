﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindCloset : MonoBehaviour
{
	SphereCollider sphere;
	KdTree<Transform> probalyTarget;
	Transform player;


	public void Initialize(GameObject _player, float attackRadius)
    {
		player = _player.transform;

		GameObject myGameobject = new GameObject("SphereCollider");
		sphere = myGameobject.AddComponent<SphereCollider>();
		myGameobject.tag = "SphereCollider";
		myGameobject.transform.SetParent(player);
		myGameobject.transform.localPosition = Vector3.zero;
		myGameobject.transform.localEulerAngles = Vector3.zero;

		sphere.enabled = false;
		sphere.isTrigger = true;
		sphere.center = Vector3.zero;
		sphere.radius = attackRadius;
	}

	public void StartSearching()
	{
		probalyTarget = new KdTree<Transform>();
		sphere.enabled = true;
	}

	public Transform Search()
	{
		if (!sphere.enabled)
		{
			return null;
		}

		Transform target = null; 

		if (probalyTarget.Count > 0)
		{
			List<Transform> targets = new List<Transform>();
			Transform t = probalyTarget.FindClosest(player.position);

			targets.Add(t);
			probalyTarget = Remove(t);

			t = VerifyClosest(targets);

			while (t != null)
			{
				t = VerifyClosest(targets);
			}

			if (targets.Count > 1)
			{
				target = GetClosestDir(targets.ToArray());
			}
			else if (targets.Count > 0)
			{
				target = targets[0];
			}
		}

		sphere.enabled = false;
		return target;
	}

	Transform VerifyClosest(List<Transform> targets)
	{
		if (probalyTarget.Count > 0 && targets.Count > 0)
		{
			Transform newCloset = probalyTarget.FindClosest(player.position);

			float dis = Vector3.Distance(targets[0].position, player.position);
			float currentDis = Vector3.Distance(newCloset.position, player.position);

			if (currentDis < dis)
			{
				Debug.LogError("You not Find the Closet before");
				return null;
			}
			else if (currentDis == dis)
			{
				targets.Add(newCloset);
				probalyTarget = Remove(newCloset);

				return newCloset;
			}
			else
			{
				return null;
			}
		}
		else
		{
			return null;
		}
	}

	KdTree<Transform> Remove(Transform toRemove)
	{
		List<Transform> list = probalyTarget.ToList();
		list.Remove(toRemove);

		KdTree<Transform> kdTree = new KdTree<Transform>();
		kdTree.AddAll(list);
		return kdTree;
	}

	Transform GetClosestDir(Transform[] targets)
	{
		float[] disDir = new float[targets.Length]; //Distance to direction
		float currentDis = 0;
		Transform closest = targets[0];

		for (int i = 0; i < disDir.Length; i++)
		{
			if (i < disDir.Length)
			{
				Quaternion dir = Quaternion.LookRotation(targets[i].position, Vector3.up);
				disDir[i] = Mathf.Abs(player.eulerAngles.y - dir.eulerAngles.y);

				if (i == 0)
				{
					currentDis = disDir[0];
					closest = targets[0];
				}
				else if (disDir[i] < currentDis)
				{
					currentDis = disDir[i];
					closest = targets[i];
				}
			}
		}

		return closest;
	}

	void OnTriggerEnter(Collider hit)
	{
		if (hit.CompareTag("Player") && hit.gameObject != player.gameObject)
		{
			probalyTarget.Add(hit.transform);
		}
	}
}
