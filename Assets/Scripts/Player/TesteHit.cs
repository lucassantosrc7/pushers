﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class TesteHit : MonoBehaviourPun
{
	Rigidbody rb;

	public GameObject player;

	void Start()
	{
		rb = GetComponent<Rigidbody>();
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			player.GetPhotonView().RPC("Hit", RpcTarget.All);
		}
	}

	void HitStrong(object[] objs)//Transform, float, ForceMode
	{
		Transform originAtk = (Transform)objs[0];
		float dis = (float)objs[1];
		rb.AddForce(originAtk.TransformDirection(Vector3.forward) * dis, (ForceMode)objs[2]);
		print("AAAAAAAAAAAAAA");
	}

	void Hit()
	{
		print("percent");
	}
}
