﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class DetectHit : MonoBehaviourPun
{
    [HideInInspector]
    public Player player;

    [Range(0,1)]
    public float decreasePercent;

    float percent = 0;

    [HideInInspector]
    public AudioClip[] clipsDamage;

	void Start()
	{
		this.enabled = Player.CheckDisable(player);
	}

	void Update()
	{
		
	}

	[PunRPC]
	void HitStrong(Vector3 dir, float distance, int forceMode)//Transform, float, ForceMode
	{
		Player.PlayerSound(clipsDamage, player.source);

		float dis = distance * (1 + percent);
		player.rb.AddForce(dir * dis, (ForceMode)forceMode);
		Percent();
    }

	[PunRPC]
	void Hit()
	{
		Percent();
	}

	void Percent()
	{
		percent += decreasePercent;
		float finalpercent = percent * 10;
		player.percent.text = finalpercent.ToString("0") + "0%";
	}
}
