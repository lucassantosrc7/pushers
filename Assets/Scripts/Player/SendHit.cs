﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]
public class SendHit : MonoBehaviour
{
	[HideInInspector]
	public Attack attack;

	Collider col;
	Rigidbody rb;

	void Start()
	{
		col = GetComponent<Collider>();
		rb = GetComponent<Rigidbody>();

		col.isTrigger = true;

		rb.useGravity = false;
		rb.isKinematic = true;
	}

	void OnTriggerEnter(Collider hit)
	{
		if (hit.CompareTag("Player") 
		&&  hit.gameObject != attack.gameObject)
		{
			attack.Hitted(hit.transform);
		}
	}
}
