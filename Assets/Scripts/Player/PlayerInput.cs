﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Player))]
public class PlayerInput : MonoBehaviour
{
	Player player;
    Movement movement;
	GameController gameController;

	EventTriggerFunctions buttonAtk
	{
		get
		{
			player.buttonAtk = gameController.buttonAtk;
			return gameController.buttonAtk;
		}
		set
		{
			gameController.buttonAtk = value;
			player.buttonAtk = gameController.buttonAtk;
		}
	}

	EventTriggerFunctions buttonMove
	{
		get
		{
			player.buttonMove = gameController.buttonMove;
			return gameController.buttonMove;
		}
		set
		{
			gameController.buttonMove = value;
			player.buttonMove = gameController.buttonMove;
		}
	}

	EventTriggerFunctions buttonSkill
	{
		get
		{
			player.buttonSkill = gameController.buttonSkill;
			return gameController.buttonSkill;
		}
		set
		{
			gameController.buttonSkill = value;
			player.buttonSkill = gameController.buttonSkill;
		}
	}

	void Start()
    {
		player = GetComponent<Player>();
		if (!player.photonView.IsMine)
		{
			this.enabled = false;
			return;
		}

		gameController = GameController.Instance;
        movement = GetComponent<Movement>();

		buttonAtk = gameController.buttonAtk;
		buttonMove = gameController.buttonMove;
		buttonSkill = gameController.buttonSkill;
	}

    void Update()
    {
        #region prints
        /*
        print("ATK: function: " + buttonAtk.state + " player: " + player.buttonAtk.state + " GameController: " + gameController.buttonAtk.state);
		print("Move: function: " + buttonMove.state + " player: " + player.buttonMove.state + " GameController: " + gameController.buttonMove.state);
		print("Skill: function: " + buttonSkill.state + " player: " + player.buttonSkill.state + " GameController: " + gameController.buttonSkill.state);
        */
        #endregion

        movement.Move(gameController.joystick.Direction);
    }
}
