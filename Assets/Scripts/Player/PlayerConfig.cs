﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerConfig : MonoBehaviour
{
    [HideInInspector]
    public Player player;

	[SerializeField]
	Behaviour[] behaviours;

	[Space(20)]
	public Sprite photo;

    #region Movement
    [Header("Movement")]
    [SerializeField]
    bool changeSpeed = false;

    [ConditionalHide("changeSpeed", true)]
    [SerializeField]
    float speed = 5;
    #endregion

    #region DetectHit
    [Header("DetectHit")]
    [SerializeField]
    bool changeDecreasePercent = false;

    [ConditionalHide("changeDecreasePercent", true)]
    [SerializeField]
    [Range(0, 1)]
    float decreasePercent = 0;

    [SerializeField]
    AudioClip[] clipsDamage;
    #endregion

    #region Attack
    [Header("Attack")]
    [SerializeField]
    bool changeWeaponSize = false;

    [ConditionalHide("changeWeaponSize", true)]
    [SerializeField]
    float weaponSize = 2;

    [SerializeField]
    bool changeDistanceBack = false;

    [ConditionalHide("changeDistanceBack", true)]
    [SerializeField]
    float distanceBack = 2;

	[SerializeField]
	bool changeNumHit = false;

	[ConditionalHide("changeNumHit", true)]
	[SerializeField]
	int numHit = 3;

	[Space(10)]
	[SerializeField]
	SendHit weapon;

	[SerializeField]
    AudioClip[] clipsHit, clipsMiss;
	#endregion

	[Header("Selection")]
	[SerializeField]
	AudioClip[] clipPick;

	void Awake()
	{
		EnableBehaviours(false);
	}

    /*void WakeUp(Player hero)
    {
		EnableBehaviours(false);

		player = Instantiate(hero, transform.position, Quaternion.identity);
		player.character = this;
		transform.SetParent(player.transform);

		player.Initialize(photonView.IsMine);

		EnableBehaviours(true);
	}*/

	public void CheckChange(Player hero)
	{
		EnableBehaviours(false);

		player = hero;

		#region Change movement
		if (changeSpeed)
		{
			player.movement.speed = speed;
		}
		#endregion

		#region Change detectHit
		if (changeDecreasePercent)
		{
			player.detectHit.decreasePercent = decreasePercent;
		}

		player.detectHit.clipsDamage = clipsDamage;
		#endregion

		#region Change attack
		if (changeWeaponSize)
		{
			player.attack.weaponSize = weaponSize;
		}

		if (changeDistanceBack)
		{
			player.attack.distanceBack = distanceBack;
		}

		if (changeNumHit)
		{
			player.attack.numHit = numHit;
		}

		player.attack.clipsHit = clipsHit;
		player.attack.clipsMiss = clipsMiss;
		player.attack.weapon = weapon;
		#endregion

		EnableBehaviours(true);
	}

	void Update()
    {
        
    }

	void YouSelected(AudioSource source)
	{
		if(clipPick.Length <= 0)
		{
			return;
		}

		source.PlayOneShot(clipPick[Random.Range(0, clipPick.Length)]);
	}

	void EnableBehaviours(bool _enabled)
	{
		for (int i = 0; i < behaviours.Length; i++)
		{
			behaviours[i].enabled = _enabled;
		}
	}
}