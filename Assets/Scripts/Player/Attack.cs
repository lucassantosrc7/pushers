﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Attack : MonoBehaviourPun
{
	public enum AttackForce
	{
		Middle, Strong
	}

	AttackForce attackForce;

	[HideInInspector]
    public Player player;

	[HideInInspector]
	public SendHit weapon;
	public float weaponSize = 2;
    public float distanceBack = 2;

    bool atkAnim = false;

    [HideInInspector]
    public AudioClip[] clipsHit, clipsMiss;

	[SerializeField]
	float attackRadius = 1;
	FindCloset findCloset;
	Transform target;

	#region Combo
	public int numHit;
	int currentNumHit;
	int currentNumAnim;

	float cancelAtk;
	bool toAttack = false;
	#endregion

	void Start()
    {
		this.enabled = Player.CheckDisable(player);

		findCloset = this.gameObject.AddComponent<FindCloset>();
		findCloset.Initialize(gameObject, attackRadius);

		currentNumHit = 0;
		currentNumAnim = 0;

		weapon.attack = this;
	}

	void Update()
    {
		if (player.buttonAtk.state == EventTriggerFunctions.Functions.Down)
        {
			findCloset.StartSearching();
			Player.PlayerSound(clipsMiss, player.source);
			if(currentNumHit <= 0)
			{
				player.anim.SetBool("Atk", true);
			}
			Combo();
            player.buttonAtk.state = EventTriggerFunctions.Functions.Null;
        }
        else
        {
			target = findCloset.Search();
			player.anim.SetBool("Atk", false);
		}

		#region LooktoTarget
		if (target != null)
        {
            Transform character = player.character.transform;
            Quaternion dir = Quaternion.LookRotation(target.position - character.position);
            float finalAngle = Quaternion.LookRotation(target.position).eulerAngles.y;

            if (Mathf.Abs(character.eulerAngles.y - finalAngle) <= 1.5f
            ||  player.buttonMove.state == EventTriggerFunctions.Functions.Down)
            {
                target = null;
                player.buttonMove.state = EventTriggerFunctions.Functions.Null;
            }
            else
            {
                // the second argument, upwards, defaults to Vector3.up
                Vector3 euler = character.eulerAngles;
                Quaternion rot = Quaternion.Slerp(character.rotation, dir, Time.deltaTime);
                euler.y = rot.eulerAngles.y;

                character.eulerAngles = euler;
            }
        }
		#endregion
	}

	public void Combo()
	{
		currentNumHit++;
		player.anim.SetInteger("Attack", currentNumHit);

		float timeAnim = player.anim.GetCurrentAnimatorClipInfo(0)[0].clip.length;
		StartCoroutine(AnimFinished(timeAnim));
	}

	public void Hitted(Transform hit)
	{
		if (!toAttack)
		{
			return;
		}

		Player.PlayerSound(clipsHit, player.source);
		if (attackForce == AttackForce.Strong)
		{
			SendHitStrong(player.character.transform, hit, distanceBack, ForceMode.Impulse);
			attackForce = AttackForce.Middle;
		}
		else
		{
			SendHit(hit);
		}
	}

	public static void SendHitStrong(Transform offender, Transform assaulted, float _distanceBack, ForceMode force)
	{
		Vector3 dir = offender.TransformDirection(Vector3.forward);
		assaulted.gameObject.GetPhotonView().RPC("HitStrong", RpcTarget.All, dir, _distanceBack, (int) force);
	}

	public static void SendHit(Transform assaulted)
	{
		assaulted.gameObject.GetPhotonView().RPC("Hit", RpcTarget.All);
	}

	IEnumerator AnimFinished(float t)
	{
		toAttack = true;
		yield return new WaitForSeconds(t);
		toAttack = false;
		currentNumAnim++;

		if (currentNumHit >= numHit 
	    && currentNumAnim == numHit - 1)
		{
			attackForce = AttackForce.Strong;
		}
		else if(currentNumAnim < numHit - 1)
		{
			attackForce = AttackForce.Middle;
		}

		if (currentNumAnim == currentNumHit)
		{
			currentNumHit = 0;
			currentNumAnim = 0;
			player.anim.SetInteger("Attack", currentNumHit);
		}
	}
}
