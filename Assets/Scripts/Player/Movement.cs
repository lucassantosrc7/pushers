﻿using UnityEngine;

public class Movement : MonoBehaviour
{
    [HideInInspector]
    public Player player;

    public float speed = 5;

    Transform character;
	Animator characterAnim;

	void Start()
	{
		this.enabled = Player.CheckDisable(player);

		if (this.enabled)
		{
			character = player.character.transform;
			characterAnim = character.GetComponentInChildren<Animator>();
		}
		else
		{
			return;
		}
	}

    public void Move(Vector2 dir)
    {
        Vector3 finalDir = new Vector3(dir.x, 0, dir.y) * -1;
        player.rb.MovePosition(transform.position + finalDir * speed * Time.deltaTime);
        characterAnim.SetFloat("CurrentMoveSpeed", dir.magnitude * speed);

        if (dir != Vector2.zero)
        {
            character.rotation = Quaternion.Euler(0, Mathf.Atan2(finalDir.z, -finalDir.x) * Mathf.Rad2Deg - 90.0f, 0);
        }
    }
}
