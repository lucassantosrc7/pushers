﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerConfig))]
public class Seahorse : MonoBehaviour
{
    Player player;
	PlayerConfig playerConfig;

    [SerializeField]
    float distanceBack = 2;

    [SerializeField]
    float distance = 2;

    [SerializeField]
    AudioClip[] clipsUlt;

    [SerializeField]
    ParticleSystem water;

	void Awake()
	{
		playerConfig = GetComponent<PlayerConfig>();
		water.Stop();
	}

	void OnEnable()
	{
		if (player != null)
		{
			return;
		}

		if (playerConfig == null)
		{
			playerConfig = GetComponent<PlayerConfig>();
		}

		if (playerConfig.player != null)
		{
			player = playerConfig.player;
		}
	}

	void Update()
    {
        if (player.buttonSkill.state == EventTriggerFunctions.Functions.Down)
        {
			RaycastHit hit;

			player.character.transform.eulerAngles = new Vector3(0, player.character.transform.eulerAngles.y, 0);
			// Does the ray intersect any objects excluding the player layer
			if (Physics.Raycast(transform.position, player.character.transform.TransformDirection(Vector3.forward), out hit, distance))
			{
				object[] objs = new object[] { distanceBack, ForceMode.Acceleration };
				hit.transform.SendMessage("Hit", objs, SendMessageOptions.DontRequireReceiver);
			}

            water.Play();
			Player.PlayerSound(clipsUlt, player.source);
			player.anim.SetBool("Ult", true);
            player.buttonSkill.state = EventTriggerFunctions.Functions.Null;
        }
        else
        {
            player.anim.SetBool("Ult", false);
        }
    }
}
