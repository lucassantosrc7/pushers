﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Horned : MonoBehaviour
{
    [HideInInspector]
    public Player player;

    [SerializeField]
    float multiply = 3;

    Collider coll;

    void Start()
    {
        coll = GetComponent<Collider>();
        coll.isTrigger = true;
    }

    void OnTriggerEnter(Collider hit)
    {
		if(hit.gameObject != player.gameObject)
		{
			object[] objs = new object[] { (player.character.transform, player.attack.distanceBack * multiply), ForceMode.Impulse };
			hit.transform.SendMessage("Hit", objs, SendMessageOptions.DontRequireReceiver);
		}
    }
}
