﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buffalo : MonoBehaviour
{
    Player player;
	PlayerConfig playerConfig;

    float t;
    Vector3 startPosition;
    Vector3 target;
    float timeToReachTarget;

    [SerializeField]
    Horned horned;

    [SerializeField]
    float distance;

    [Header("Time")]
    [SerializeField]
    float attack;
    [SerializeField]
    float anticipation;

    bool attacking = false;

    [SerializeField]
    AudioClip[] clipsUlt;

	void Awake()
	{
		playerConfig = GetComponent<PlayerConfig>();
	}

	void OnEnable()
	{
		if (player != null)
		{
			return;
		}

		if(playerConfig == null)
		{
			playerConfig = GetComponent<PlayerConfig>();
		}

		if(playerConfig.player != null)
		{
			player = playerConfig.player;
			horned.player = playerConfig.player;
		}
	}

	void Update()
    {
        if (attacking)
        {
            float disTarget = Vector3.Distance(player.transform.position, target);
            if (disTarget <= 0.2f)
            {
                attacking = false;
            }
            else
            {
                t += Time.deltaTime / timeToReachTarget;
                player.transform.position = Vector3.Lerp(startPosition, target, t);
            }
        }
        else if (player.buttonSkill.state == EventTriggerFunctions.Functions.Down)
        {
			Player.PlayerSound(clipsUlt, player.source);
            player.anim.SetBool("Ult", true);
            player.buttonSkill.state = EventTriggerFunctions.Functions.Null;

            StartCoroutine(Anticipation());
        }
        else
        {
            player.anim.SetBool("Ult", false);
        }
    }

    public void SetDestination(Vector3 destination, float time)
    {
        t = 0;
        startPosition = player.transform.position;
        timeToReachTarget = time;
        target = destination;

        attacking = true;
    }

    IEnumerator Anticipation()
    {
        yield return new WaitForSeconds(anticipation);
        Vector3 destination = transform.position;
        destination += transform.TransformDirection(Vector3.forward * distance);
        SetDestination(destination, attack);
    }
}
