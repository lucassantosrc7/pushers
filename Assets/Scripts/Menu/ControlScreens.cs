﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlScreens : MonoBehaviour
{
    public static ControlScreens instance { get; private set; }

    public enum Screens
    {
        Game, Menu, Store, Connecting, WinScreen
    }
    [SerializeField]
    Screens first_Screen;

    public static Screens currentScreen;

	public Camera menuCam;

	[Header("Screens")]
    public GameObject menu;
    public GameObject game, store, connecting, winScreen;

    AudioSource source;

    [SerializeField]
    AudioClip buttonClip;

	void Start()
	{
		instance = this;
		source = GetComponent<AudioSource>();

		menu.SetActive(false);
		game.SetActive(false);
		store.SetActive(false);
		connecting.SetActive(false);
		winScreen.SetActive(false);

		ChangeScreen(first_Screen);
	}

    public void ChangeScreen(Screens newScreen)
    {
        switch (currentScreen)
        {
            case Screens.Menu:
                menu.SetActive(false);
				MenuCam(false);
                break;
            case Screens.Game:
                game.SetActive(false);
				MenuCam(true);
				break;
            case Screens.Store:
                store.SetActive(false);
				MenuCam(false);
				break;
            case Screens.Connecting:
                connecting.SetActive(false);
				MenuCam(false);
				break;
			case Screens.WinScreen:
				winScreen.SetActive(false);
				MenuCam(false);
				break;
		}

        switch (newScreen)
        {
            case Screens.Menu:
                menu.SetActive(true);
				MenuCam(true);
				break;
            case Screens.Game:
				GameController.Instance.PreparingGame();
                game.SetActive(true);
				MenuCam(false);
				break;
            case Screens.Store:
                store.SetActive(true);
				MenuCam(true);
				break;
            case Screens.Connecting:
                connecting.SetActive(true);
				MenuCam(true);
				break;
			case Screens.WinScreen:
				winScreen.SetActive(true);
				MenuCam(true);
				break;
		}

        currentScreen = newScreen;
    }

	void MenuCam(bool active)
	{
		if (active && menuCam.gameObject.activeInHierarchy
	    || (!active && !menuCam.gameObject.activeInHierarchy))
		{
			return;
		}

		menuCam.gameObject.SetActive(active);
	}

    public void GoToGame()
    {
        if (buttonClip != null && !source.isPlaying)
        {
            source.PlayOneShot(buttonClip);
        }
        ChangeScreen(Screens.Game);
    }
    public void GoToMenu()
    {
        if (buttonClip != null && !source.isPlaying)
        {
            source.PlayOneShot(buttonClip);
        }
        ChangeScreen(Screens.Menu);
    }
    public void GoToStore()
    {
        if (buttonClip != null && !source.isPlaying)
        {
            source.PlayOneShot(buttonClip);
        }
        ChangeScreen(Screens.Store);
    }
}
