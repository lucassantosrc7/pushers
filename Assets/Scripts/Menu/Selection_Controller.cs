﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selection_Controller : MonoBehaviour
{

	[SerializeField]
	Transform selectedPostion, viewPosition;
	[SerializeField]
	GameObject turtle, phoenix, bull, horsea;

	[SerializeField]
	AudioSource source;

	GameObject currentSelected, currentView;
	string currentName;

	void Start()
	{
		ViewInstantiate(horsea, "Cesar");
		SelectInstantiate();
	}


	public void viewHorsea()
	{
		ViewInstantiate(horsea, "Cesar");
	}

	public void viewBull()
	{
		ViewInstantiate(bull, "Bill");
	}

	public void viewPhoenix()
	{
		ViewInstantiate(phoenix, "Phoenix");
	}

	public void viewTurtle()
	{
		ViewInstantiate(turtle, "Turtle");
	}

	public void SelectChamp()
	{
		SelectInstantiate();
		currentSelected.SendMessage("YouSelected", source, SendMessageOptions.DontRequireReceiver);
	}

	void SelectInstantiate()
	{
		GameController.hero = SelectHero.FindHero(currentName);

		Destroy(currentSelected);
		currentSelected = Instantiate(currentView, selectedPostion.transform);
		currentSelected.transform.localPosition = Vector3.zero;
		currentSelected.transform.localEulerAngles = Vector3.zero;
		currentSelected.transform.localScale = Vector3.one;
	}

	void ViewInstantiate(GameObject hero, string heroName)
	{
		currentName = heroName;

		Destroy(currentView);
		currentView = Instantiate(hero, viewPosition.transform);
		currentView.transform.localPosition = Vector3.zero;
		currentView.transform.localEulerAngles = Vector3.zero;
		currentView.transform.localScale = Vector3.one;
	}
}

