﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateChamp : MonoBehaviour
{
    public int speed = 10;

    private void OnMouseDrag()
    {
        transform.Rotate(new Vector3(0, -Input.GetAxis("Mouse X"), 0) * Time.deltaTime * speed);
    }
}
