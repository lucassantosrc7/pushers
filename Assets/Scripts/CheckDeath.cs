﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckDeath : MonoBehaviour
{
	enum Methods {
		Null, DeathStateLava
	}
	Methods methods = Methods.Null;

	bool goToMenuImmediately = false;

	Player player;

	[SerializeField]
	Camera Skycam;

	void Awake()
	{
		Skycam.gameObject.SetActive(false);
	}

	void Update()
	{
		switch (methods)
		{
			case Methods.DeathStateLava:
				player.transform.Translate(Vector3.down * Time.deltaTime);
				break;
		}
	}

	void DeathStateLava()
	{
		goToMenuImmediately = false;
		player.rb.useGravity = false;
		player.rb.isKinematic = true;

		methods = Methods.DeathStateLava;
	}

	void OnTriggerEnter(Collider hit)
	{
		if (hit.CompareTag("Player"))
		{
			player = hit.GetComponent<Player>();
			if (player.photonView.IsMine)
			{
				goToMenuImmediately = true;

				string methodName = "DeathState" + GameController.scenario.scenarioName;
				gameObject.SendMessage(methodName, SendMessageOptions.DontRequireReceiver);

				if (goToMenuImmediately)
				{
					GoToMenu();
				}
			}
		}
	}

	void GoToMenu()
	{
		if(player == null)
		{
			return;
		}

		player.gameObject.SetActive(false);
		methods = Methods.Null;
		GameController gC = GameController.Instance;
		gC.photonView.RPC("RemovePlayer", Photon.Pun.RpcTarget.All);

		Skycam.gameObject.SetActive(true);
	}

	void OnTriggerExit(Collider hit)
	{
		GoToMenu();
	}

	IEnumerator GoToMenuImmediately(float t)
	{
		yield return new WaitForSeconds(t);
		GoToMenu();
	}
}
