﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLava : MonoBehaviour
{

    float ScrollX = 0.5f;
    float ScrollY = 0;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float OffsetX =  ScrollX * Time.time;
        float OffsetY = ScrollY;
        GetComponent<Renderer>().material.mainTextureOffset = new Vector2(OffsetX, OffsetY);
    }
}
