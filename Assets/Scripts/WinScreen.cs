﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinScreen : MonoBehaviour
{
	[SerializeField]
	Text nameChar, money;

	[SerializeField]
	Transform charPos;

	[SerializeField]
	Image title;

	[SerializeField]
	Sprite winTitle, LostTitle;

	[SerializeField]
	float timeMenu = 2f;

	public void Active(bool win, Heroes hero)
	{
		ControlScreens.instance.ChangeScreen(ControlScreens.Screens.WinScreen);

		nameChar.text = hero.heroName;
		Transform player = Instantiate(hero.hero.character, charPos).transform;
		player.localPosition = Vector3.zero;
		player.localEulerAngles = Vector3.zero;

		if (win)
		{
			title.sprite = winTitle;
			money.text = "40";
			player.GetComponent<Animator>().SetBool("Win", true);
		}
		else
		{
			title.sprite = LostTitle;
			money.text = "10";
			player.GetComponent<Animator>().SetBool("Lost", true);
		}
	}

	public void Menu()
	{
		Photon.Pun.PhotonNetwork.Disconnect();
	}
}
