﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Heroes", menuName = "CapMultidimensional/Heroes", order = 1)]
public class HeroesConfig : ScriptableObject
{
    public Heroes [] heroes;
}

[System.Serializable]
public class Heroes
{
    public string heroName;
    public Player hero;
}

