﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectHero : MonoBehaviour
{
    [SerializeField] Transform iniMenu;

    [SerializeField] GameObject popUp;
    [SerializeField] GameObject bufalo;
    [SerializeField] GameObject cavalo;

    [SerializeField] AudioClip [] buffaloPick;
    [SerializeField] AudioClip [] housePick;

    AudioSource source;

    public static GameObject currentCharacter;

    void Start()
    {
       currentCharacter = Instantiate(bufalo, new Vector3(0, 0, -7.35f), Quaternion.Euler(0, 180, 0), iniMenu);
       source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        popUp.gameObject.SetActive(true);
        
    }

    public void instantBufalo()
    {
        if (currentCharacter.tag != "bufalo")
        {
            if (buffaloPick != null && !source.isPlaying)
            {
                source.PlayOneShot(buffaloPick[Random.Range(0, buffaloPick.Length)]);
            }
            Destroy(currentCharacter);
            currentCharacter = Instantiate(bufalo, iniMenu);
            currentCharacter.transform.position = new Vector3(0, 0, -7.35f);
            currentCharacter.transform.rotation = Quaternion.Euler(0, 180, 0);
            popUp.SetActive(false);
        }
    }

    public void instantCavalo()
    {
        if (currentCharacter.tag != "cavalo")
        {
            if (housePick != null && !source.isPlaying)
            {
                source.PlayOneShot(housePick[Random.Range(0, housePick.Length)]);
            }
            Destroy(currentCharacter);
            currentCharacter = Instantiate(cavalo, iniMenu);
            currentCharacter.transform.position = new Vector3(0, 0.9f, -7.35f);
            currentCharacter.transform.rotation = Quaternion.Euler(0, 180, 0);
            print(currentCharacter.transform.position);
            popUp.SetActive(false);
        }
    }

    public void Close()
    {
        popUp.SetActive(false);
    }

	public static Heroes FindHero(string heroName)
	{
		Heroes[] heroes = GameController.Instance.heroesConfig.heroes;

		for (int i = 0; i < heroes.Length; i++)
		{
			if (heroes[i].heroName == heroName)
			{
				Heroes hero = new Heroes();
				hero.heroName = heroes[i].heroName;
				hero.hero = heroes[i].hero;
				return hero;
			}
		}

		Debug.LogError("Not find the scenario");
		return null;
	}
}
