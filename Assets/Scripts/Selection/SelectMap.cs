﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectMap : MonoBehaviour
{
	GameController gameController;
	ScenariosConfig scenarios;

	[SerializeField] Image mapImage;
    [SerializeField] GameObject popUpMaps;
    [SerializeField] BoxCollider SelectHero;

	Scenarios currentMap
	{
		get
		{
			return GameController.scenario;
		}
		set
		{
			GameController.scenario = value;
		}
	}

	void Start()
    {
		currentMap = scenarios.scenarios[0];
        mapImage.sprite = currentMap.sprite;

        for(int i = 0; i < scenarios.scenarios.Length; i++)
        {
            scenarios.scenarios[i].scenario.SetActive(false);
        }
    }

    public void abrePopUp()
    {
        popUpMaps.SetActive(true);
        SelectHero.enabled = false;
    }

    public void Map1()
    {
        if(currentMap.sprite != scenarios.scenarios[0].sprite)
        {
            currentMap = scenarios.scenarios[0];
            mapImage.sprite = currentMap.sprite;
            popUpMaps.SetActive(false);
            SelectHero.enabled = true;
        }
    }

    public void Map2()
    {
        if (currentMap.sprite != scenarios.scenarios[1].sprite)
        {
            currentMap = scenarios.scenarios[1];
            mapImage.sprite = currentMap.sprite;
            popUpMaps.SetActive(false);
            SelectHero.enabled = true;
        }
    }

    public void ClosePopUp()
    {
        popUpMaps.SetActive(false);
        SelectHero.enabled = true;
    }

	public static Scenarios FindScenario(string scenarioName)
	{
		Scenarios[] scenarios = GameController.Instance.scenariosConfig.scenarios;

		for (int i = 0; i < scenarios.Length; i++)
		{
			if(scenarios[i].scenarioName == scenarioName)
			{
				Scenarios scenario = new Scenarios();
				scenario.scenario = scenarios[i].scenario;
				scenario.scenarioName = scenarios[i].scenarioName;
				scenario.sprite = scenarios[i].sprite;

				return scenario;
			}
		}

		Debug.LogError("Not find the scenario");
		return null;
	}
}
