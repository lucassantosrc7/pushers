﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Scenarios", menuName = "CapMultidimensional/Scenarios", order = 2)]
public class ScenariosConfig : ScriptableObject {
	public Scenarios[] scenarios;
}

[System.Serializable]
public class Scenarios
{
	public string scenarioName;
	public GameObject scenario;
	public Sprite sprite;

	public List<Transform> GetBornPlaces()
	{
		List<Transform> bornPlace = new List<Transform>();

		int numchild = scenario.transform.childCount;

		for (int i = 0; i <= numchild; i++)
		{
			Transform child = scenario.transform.GetChild(i);
			if (child.CompareTag("BornPlace"))
			{
				bornPlace = new List<Transform>();
				for (int j = 0; j < child.childCount; j++)
				{
					bornPlace.Add(child.GetChild(j));
				}

				i = numchild + 1;
			}
			else if(i >= numchild)
			{
				Debug.LogError("Need a BornPlace, check if you put the correct Tag");
			}
		}

		return bornPlace;
	}
}
