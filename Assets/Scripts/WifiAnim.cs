﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WifiAnim : MonoBehaviour
{
	[SerializeField]
	Sprite[] sprites;

	[SerializeField]
	UnityEngine.UI.Image image;

	int currentSprite;
	[SerializeField]
	float time;
	float t;

    void Start()
    {
		currentSprite = 0;
		image.sprite = sprites[currentSprite];
		t = Time.time + time;
	}

	void Update()
	{
		if(Time.time > t)
		{
			currentSprite++;
			if (currentSprite >= sprites.Length)
			{
				currentSprite = 0;
			}

			image.sprite = sprites[currentSprite];
			t = Time.time + time;
		}
	}
}
