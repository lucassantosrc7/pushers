﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class topMenuController : MonoBehaviour
{

    [SerializeField]
    GameObject topMenu;

    public void Active()
    {
        topMenu.gameObject.SetActive(true);
    }

    public void Desactive()
    {
        topMenu.gameObject.SetActive(false);
    }

}
